#include "pushdown.h"

void Pushdown::add_state(const std::string &name, bool final_state) {
  if (name == "") throw std::logic_error{"undefined state"};
  states.push_back(final_state);
  names[name] = states.size() -1;
}

void Pushdown::set_init(const std::string &name) {
  try {
    init = names.at(name);
  } catch (const std::out_of_range) {
    throw std::out_of_range{"undefined initial state"};
  }
}

void Pushdown::add_transition(const std::string &actual_state,
                      const char condition,
                      const std::string &top_stack,
                      const std::string &push_stack,
                      const std::string &next_state) {
  try {
    transition[names.at(actual_state)]
              [condition]
              [top_stack]
              .push_back(std::make_pair(push_stack, names.at(next_state)));
  } catch (const std::out_of_range) {
    throw std::out_of_range{"undefined transition"};
  }
}

void Pushdown::epsilon_transition(std::vector<std::pair<unsigned, std::string>>
                                  &actual_states) const {
  for (auto i = 0; i < actual_states.size(); i++) {
    try {
      auto result = transition.at(actual_states[i].first).at(0); 
      for (auto it = result.begin(); it != result.end(); it++) {
        if(it->first == actual_states[i]
                        .second.substr(actual_states[i].second.size()-it->first.size(),
                                       it->first.size())) {
          for (auto next_state : it->second) {
            actual_states.push_back(std::make_pair(next_state.second,
                                    actual_states[i].second.substr(0,
                                    actual_states[i].second.size()-it->first.size())
                                    +next_state.first)); 
          }
        }
      }
    } catch (const std::out_of_range) {}
  }
}

bool Pushdown::compute(const char input[]) const {
  std::vector<std::pair<unsigned, std::string>> actual_states{std::make_pair(init, "$")}; 
  
  for (auto i = 0; input[i]; i++) {
    epsilon_transition(actual_states);

    std::vector<std::pair<unsigned, std::string>> next_states; 

//transition[actual_state][input][top_stack]{(push_stack, next_state)...}
    for (auto state : actual_states) {
      try {
        auto result = transition.at(state.first).at(input[i]); 
        for (auto it = result.begin(); it != result.end(); it++) {
          if(it->first == state.second.substr(state.second.size()-it->first.size(),
                                              it->first.size())) {
            for (auto next_state : it->second) {
              next_states.push_back(std::make_pair(next_state.second,
                                 state.second.substr(0,
                                 state.second.size()-it->first.size())+next_state.first)); 
            }
          }
        }
      } catch (const std::out_of_range) {} 
    }

    if (next_states.empty()) return false;

    actual_states = next_states;
  }

  epsilon_transition(actual_states);

  for (auto state : actual_states) {
    if (states[state.first]) return true;
  }

  return false;
}

void Pushdown::tokenize_state(const std::string &line) {
  std::stringstream line_stream{line};
  std::string token;

  bool have_init{false};
  bool have_final{false};

  while (std::getline(line_stream, token, ',')) {
    bool final_state{false};
    
    if (token[token.size()-1] == '*') {
      have_final = true;
      final_state = true;
      token = token.substr(0, token.size()-1);
    }

    if (token.substr(0, 2) == "->") {
      if (have_init == true) throw std::logic_error{"More than one initial state"};

      have_init = true;
      token = token.substr(2, token.size()-2);

      add_state(token, final_state);
      set_init(token);

    } else {
      add_state(token, final_state);
    }
  }

  if (not have_init) throw std::logic_error{"without final state"};
  if (not have_final) throw std::logic_error{"without initial state"};
}

void Pushdown::tokenize_transition(const std::string &line) {
  std::stringstream line_stream{line};
  std::string token;

  while (std::getline(line_stream, token, ',')) {
    std::stringstream token_stream{token};
    
    std::string actual_states;
    std::string condition;
    std::string top_stack;
    std::string push_stack;
    std::string next_state;
  
    std::getline(token_stream, actual_states, ' ');
    std::getline(token_stream, condition, ' ');
    if (condition.size() > 1)
      throw std::logic_error{"transition condition must just have one character"};
    std::getline(token_stream, top_stack, ' ');
    std::getline(token_stream, push_stack, ' ');
    std::getline(token_stream, next_state, ' ');

    add_transition(actual_states, condition[0] == '_'? 0 : condition[0],
                   top_stack == "_"? "" : top_stack,
                   push_stack == "_"? "" : push_stack, next_state);
  }
}

void Pushdown::tokenize(const char path[]) {
  std::ifstream file{path};
  std::string line;

  std::getline(file, line, ';');
  tokenize_state(std::regex_replace(line, std::regex("\\s+"), ""));

  std::getline(file, line, ';');
  line = std::regex_replace(line, std::regex("\\n"), "");
  line = std::regex_replace(line, std::regex("  "), " ");
  line = std::regex_replace(line, std::regex("  "), " ");
  tokenize_transition(line);
}

Pushdown::Pushdown(const char path[]) {
  tokenize(path);
}
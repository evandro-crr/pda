#pragma once
#include <regex>
#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <utility>
#include <unordered_map>

class Pushdown {
 public:
  Pushdown(const char path[]);
  void add_state(const std::string &name, bool final_state);
  void set_init(const std::string &name);
  void add_transition(const std::string &actual_state,
                      const char condition,
                      const std::string &top_stack,
                      const std::string &push_stack,
                      const std::string &next_state);
  bool compute(const char input[]) const;
  
 private:
  void epsilon_transition(std::vector<std::pair<unsigned, std::string>> &actual_states)
  const;

  void tokenize(const char path[]);
  void tokenize_state(const std::string &line);
  void tokenize_transition(const std::string &line);

  std::vector<bool> states;
  std::unordered_map<std::string, unsigned> names;
  unsigned init;
//transition[actual_state][input][top_stack]{(push_stack, next_state)...}
  std::unordered_map<unsigned,
                     std::unordered_map<char,
                     std::unordered_map<std::string,
                     std::vector<std::pair<std::string, unsigned>>>>> transition;
};
